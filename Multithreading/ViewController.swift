//
//  ViewController.swift
//  Multithreading
//
//  Created by Yaroslav Krasnokutskiy on 7.9.23..
//

// NSThread
// NSOperations
// Semaphores
// GCD
// async/await

import UIKit

class ViewController: UIViewController {
    // Threads
    var thread: Thread!
    // Queues
    var mainQueue: DispatchQueue!
    var concurrentQueue: DispatchQueue!
    var serialQueue: DispatchQueue!
    var operationQueue: OperationQueue!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initiationOfThreads()
        initiationOfQueues()
    }
    
    private func initiationOfThreads() {
        // initiating thread with Thread factoryMethod
        // execute immediately
        Thread.detachNewThreadSelector(#selector(threadUsingFactory), toTarget: self, with: nil)
        
        // create thread
        thread = Thread(target: self, selector: #selector(threadUsingInit), object: nil)
    }
    
    private func initiationOfQueues() {
        mainQueue = DispatchQueue.main
        concurrentQueue = DispatchQueue(label: "newConcurrentQueue", attributes: .concurrent)// cuncorrent
        serialQueue = DispatchQueue(label: "newSerialQueue")// default serial
    }
    
    private func initiationOfOperations() {
        operationQueue = OperationQueue()
        for i in 0...10 {
            operationQueue.addOperation {
                print(i)
            }
        }
        operationQueue.waitUntilAllOperationsAreFinished()
        
        let mainOperation = BlockOperation{
            print("mainOperation")
        }
        let operationToStartWhenMainFinishes = BlockOperation{
            print("Dependency Operation")
        }
        mainOperation.addDependency(operationToStartWhenMainFinishes)
        if operationToStartWhenMainFinishes.isFinished {
            print("two operation has finished")
        }
        
        // we can cancel
        operationQueue.cancelAllOperations()
    }
    
    private func initiationOfOperations2() {
        final class ImageLoaderOperation: Operation {
            private(set) var result: Result<[String], Error>?
            private let urlsResult: Result<[String],Error>
            override func main() {
                // do some work with urls -> success or failure
                switch urlsResult {
                case .success(let success):
                    // do some work with success urls -> result/failure
                    let resultOfLoadingImages = success
                    result = .success(resultOfLoadingImages)
                case .failure(let failure):
                    result = .failure(failure)
                }
            }
            init(urlsResult: Result<[String],Error>){
                self.urlsResult = urlsResult
            }
        }
        
        final class ImageUrlLoaderOperation: Operation {
            private(set) var result: Result<[String], Error>?
            override func main() {
                result = .success(["a","b"])
            }
        }
        
        let operation = ImageLoaderOperation()
        let imageUrlLoaderOperation = ImageUrlLoaderOperation()
        let imageLoadingOperation = ImageLoaderOperation(urlsResult: imageUrlLoaderOperation.result ?? .success([""]))
        imageLoadingOperation.addDependency(imageUrlLoaderOperation)
        operationQueue.addOperations([imageLoadingOperation, imageUrlLoaderOperation], waitUntilFinished: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // need to call start to execute
        thread.start()
        
        //Queues
        mainQueue.async {
            self.foo()
        }
        
        concurrentQueue.sync {
            self.foo()
        }
        
        concurrentQueue.sync {
            self.foo()
        }
        
        serialQueue.async {
            self.foo()
        }
        serialQueue.sync {
            self.foo()
        }
    }
    
    private func foo() {
        print(Thread.current)
    }
    
    @objc
    func threadUsingFactory() {
        print(#function)
    }
    @objc
    func threadUsingInit() {
        print(#function)
    }
    
}

